<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Log in</title>
    <link rel="stylesheet" href="TianCSS/index_login.css">
    <script src="http://www.google.com/recaptcha/api.js" async defer></script>
    <script SRC="JQuery_lib/jquery-3.3.1.js"></script>
    <script SRC="JQuery_lib/jQuery.md5.js"></script>
    <script src="myJS.js"></script>
    <style>
        .alert-danger {
            margin: 20px auto;
            height: 35px;
            color: red;
        }
    </style>
</head>
<body>
<div id="login_frame">
    <div class="errorMsg">
        <p></p>
    </div>
    <p id="image_logo"><img src="images/icon_logo.png" width="113px"></p>

    <input placeholder="  Username " type="text" name="username" id="username" class="index_login_textfield_username"/>
    <br>
    <br>
    <input placeholder="  Password " type="password" name="password" id="password"
           class="index_login_textfield_password"/>
    <br>
    <br>
    <div id="login_control">
        <input type="button" id="btn_login" name="login" value="login"/>
        <a class="index_login_forget_password" id="forget_pwd" href="index_forget_pwd.jsp">Forgotten Pasword&quest;</a>
    </div>
    <div class="alert alert-danger">
        <strong></strong>
    </div>
</div>
</body>
<script>
    $("#btn_login").click(function () {
        $.post('/UserLoginServlet', {
            'username': $('#username').val(),
            'password': $('#password').val()
        }, function (xhr) {
            var str = xhr.split(':')
            if (str[0] == "error") {
                $(".alert-danger strong").text(str[1])
            } else {
                location.href = "/homepage.jsp";
            }
        })
    });
</script>
</html>
