<%@ page import="java.util.List" %>
<%@ page import="POJO.ArticlePOJO" %>
<%@ page import="DAO.ArticleDAO" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Articles &laquo; Admin</title>
    <link rel="stylesheet" href="../vendors/bootstrap/css/bootstrap.css">
    <link rel="stylesheet" href="../vendors/font-awesome/css/font-awesome.css">
    <link rel="stylesheet" href="../adminCSS/admin.css">
</head>
<body>


<div class="container-fluid">
    <div class="page-title">
        <h1>ALL ARTICLES</h1>
    </div>
    <!-- when have incorrect message -->
    <div class="alert alert-danger">
        <strong>error&excl;</strong>
    </div>

    <table class="table table-striped table-bordered table-hover">
        <thead>
        <tr>
            <th>Author ID</th>
            <th>Article ID</th>

            <th class="text-center">Article Title</th>
            <th class="text-center">Article Visibility</th>
            <th class="text-center" width="150">Show/Hide Article</th>
        </tr>
        </thead>
        <tbody>
        <%
            try (ArticleDAO newArticleDAO = new ArticleDAO()) {

                List<ArticlePOJO> allArticles = newArticleDAO.loadAllArticlesAdmin();

                for (ArticlePOJO a : allArticles) {
                    out.println("<tr>");
                    out.println("<td>" + a.getAuthor_id() + "</td>");
                    out.println("<td>" + a.getArticle_id() + "</td>");
                    out.println("<td>" + a.getTitle() + "</td>");
                    out.println("<td>" + a.isArticle_visibility() + "</td>");
                    out.println("<td>");
                    int articleID = a.getArticle_id();
                    out.print("<form action=\"/AdminServlet\" method=\"get\">");
                    out.print("<input type=\"submit\" value=\"Show/Hide Article\" name=\"article_visibility_button\"\">");
                    out.print("<input type=\"hidden\" name=\"articleID\" value=\"" + articleID + "\">");
                    out.print("</form>");
                    out.println("</td");
                    out.println("</tr>");

                }
            } catch (Exception e) {
                e.getMessage();
            }
        %>

        </tbody>
    </table>

</div>
</body>
</html>
