package UserServlet;

import DAO.UserDAO;
import POJO.UserPOJO;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;

/**
 * before login user homepage, check username and password is correct or not, if not back to login page
 */
public class UserLoginServlet extends HttpServlet {


    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String username = req.getParameter("username");
        String password = req.getParameter("password");

        String errMsg = "";
        if (username.equals("") || username == null) {
            errMsg = "please enter your username";
        }
        if (password.equals("") || password == null) {
            errMsg = "please enter your password";
        }
        try (UserDAO userDAO = new UserDAO()) {
            UserPOJO userPOJO = userDAO.queryEntriesByUsername(username);
            if (userPOJO == null || userPOJO.getUsername().equals("") || username.equals("")) {
                errMsg = "don't have this username";
            } else {
                if (!userPOJO.getPassword().equals(password)) {
                    errMsg = "wrong password";
                }
            }
            if (!errMsg.equals("")) {
                resp.getWriter().write("error:" + errMsg);
                return;
            } else {
                req.getSession().setAttribute("userPOJO", userPOJO);
                resp.sendRedirect("homepage.jsp");
            }

            //Setting Sessions for later use
            req.getSession().setAttribute("userPOJO", userPOJO);
            req.getSession().setAttribute("page", "allArticles");


            req.getSession().setAttribute("userID", userPOJO.getUser_id());
            req.getSession().setAttribute("firstLogin_AllArticles", true);
            req.getSession().setAttribute("firstLogin_MyArticles", true);
            req.getSession().setAttribute("current_article", 0);
            req.getSession().setAttribute("page", "allArticles");


        } catch (SQLException e) {
            req.setAttribute("error", "Your data query is wrong!");
            req.getRequestDispatcher("/promt/Error.jsp").forward(req, resp);
        } catch (Exception e) {
            req.setAttribute("error", "Your data query is wrong!");
            req.getRequestDispatcher("/promt/Error.jsp").forward(req, resp);
        }


    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req, resp);
    }
}
