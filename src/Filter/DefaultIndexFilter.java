package Filter;


import POJO.UserPOJO;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


public class DefaultIndexFilter implements Filter {

    @Override
    public void destroy() {

    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain dofil) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;
        String userIP = request.getRemoteAddr();
        String uri = request.getRequestURI();
        String path = request.getContextPath();
        if (!uri.contains("ArticleServlet") || !uri.contains(".jsp")) {// If the request is not a servlet or JSP, direct release
            dofil.doFilter(request, response);
        } else {
            if (uri.contains("Admin")) {//Filter for request for the login administrator page
                if (userIP.contains("localhost") || userIP.contains("127.0.0.1")) {
                    if (uri.contains("login.jsp")) {
                        dofil.doFilter(request, response);
                    } else {
                        if (request.getSession().getAttribute("admin") == null) {
                            request.setAttribute("error", "You don't have d login administrator!");
                            request.getRequestDispatcher("/prompt/error").forward(request, response);
                        } else {
                            dofil.doFilter(request, response);
                        }
                    }
                } else {
                    request.setAttribute("error", "You don't have the permissions to log in to this page!");
                    request.getRequestDispatcher("/prompt/error").forward(request, response);
                }
            } else if (uri.contains("newArticleSubmitForm.jsp") || uri.contains("homepage.jsp")

                    ) {//Make a judgment on the place where there is a need for user permission on the front site
                UserPOJO userPOJO = (UserPOJO) request.getSession().getAttribute("userPOJO");
                if (userPOJO == null) {
                    request.getRequestDispatcher("/prompt/error").forward(request, response);
                }
            } else {
                dofil.doFilter(request, response);
            }

        }
    }
}
