<%--
  Created by IntelliJ IDEA.
  User: Yang
  Date: 2018/10/17
  Time: 19:30
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Add &laquo; User &laquo; Admin</title>
</head>
<link rel="stylesheet" href="../vendors/bootstrap/css/bootstrap.css">
<link rel="stylesheet" href="../vendors/font-awesome/css/font-awesome.css">
<link rel="stylesheet" href="../adminCSS/admin.css">
<body>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-6"">
            <form action="/AdminUserServlet" method="post">
                <h2>add new user</h2>
                <input type="hidden" name="adminuser" value="adduser">
                <div class="form-group">
                    <label for="username">Username</label>
                    <input id="username" class="form-control" name="username" type="text" placeholder="Username">
                    <div><label id="display"><b></b></label></div>
                </div>
                <div class="form-group">
                    <label for="password">Password</label>
                    <input id="password" class="form-control" name="password" type="password"
                           placeholder="Password">
                </div>
                <div class="form-group">
                    <label for="firstname">First Name</label>
                    <input id="firstname" class="form-control" name="firstname" type="text" placeholder="First Name">
                </div>
                <div class="form-group">
                    <label for="lastname">Last Name</label>
                    <input id="lastname" class="form-control" name="lastname" type="text" placeholder="Last Name">
                </div>
                <div class="form-group">
                    <label for="dob">Birth</label>
                    <input id="dob" class="form-control" name="dob" type="date" placeholder="Date of Birth">
                </div>
                <div class="form-group">
                    <label for="email">Email</label>
                    <input id="email" class="form-control" name="email" type="email" placeholder="Email">
                </div>
                <div class="form-group">
                    <label for="description">Description</label>
                    <input id="description" class="form-control" name="description" type="text"
                           placeholder="Description">
                </div>
                <div class="form-group" id="countries">
                    <label for="countrySelect">Country (two letters)</label>
                    <input type="text" id="countrySelect" name="country" class="form-control">
                </div>
                <div class="form-group">
                    <button id="addUser" class="btn btn-primary" type="submit">ADD</button>
                </div>
            </form>
        </div>
    </div>
</div>

</body>
<script src="../JQuery_lib/jquery-3.3.1.js"></script>
<script src="../vendors/bootstrap/js/bootstrap.js"></script>
<script>

    var errMsg = 0;
    $('#username').on('blur', function () {// when entry value changes
        var username = $("#username").val();     //when username changes
        //not null
        //ajax request
        $.post(//post data to the URL
            "/checkUserNameServlet",  //url /....servlet
            {
                'username': $("#username").val(),
                'form': "username"
            }
            , function (data) {//callback function when it's successful

                var errMsg = data.toString();
                if (errMsg == 1) {
                    $('#display b').text("No  username input!");
                    $('#display b').css('color', 'red');
                } else if (errMsg == 2) {
                    $('#display b').text("Valid username");
                    $('#display b').css('color', 'green');
                } else if (errMsg == 3) {
                    $('#display b').text("username already exist");
                    $('#display b').css('color', 'red');
                }


            });

    });

    // check username when submit the form
    // if username is empty, show "username can not be empty", only post username when it's not null
    $("#addUser").click(function () {

        var username = $("#username").val();
        var email = $("#email").val();
        var reg = /^([a-zA-Z0-9_-])+@([a-zA-Z0-9_-])+((\.[a-zA-Z0-9_-]{2,3}){1,2})$/;
        var dob = $("#dob").val();

        console.log(dob);
        $.ajax({//request data

            type: "POST",
            url: "/checkUserNameServlet",  //url /....servlet
            async: false,
            data: {
                username: $("#username").val(), //data "suer"
                email:$("#email").val(),
                form: "username",
            },
            success: function (data) {//callback function when it's successful
                // console.log(data);
                errMsg = data;
                // submitTest();
            }
        });

        // function submitTest() {
        if (errMsg == 1) {
            alert("No  user name input!");
            return false;
        } else if (errMsg == 3) {
            // console.log(errMsg)
            alert("username already exist");

            return false;
        }
        //if username is null
        else if (username == null || username.length < 1) {

            alert("username can not be empty!")
            return false;
        }else if(email == null|| email.length<1){

            alert("email can not be empty!")
            return false;
        }
        else if(email !== null&&!reg.test(email)){

            alert("Please type in valid email!")
            return false;
        }else if(dob == ""){
            alert("birthday can not be empty!")
            return false;
        }
        else {
            $("form").submit();
        }
    });




</script>


</html>
