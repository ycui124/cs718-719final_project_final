<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Reset password</title>
    <link rel="stylesheet" href="TianCSS/index_forget_password.css">
    <script src="JS/myJS.js"></script>

</head>
<body>

<form action="/UserForgotPasswordServlet" method="post">
    <div id="login_frame_resetpassword">
        <p id="image_logo"><img src="images/icon_logo.png" width="113px"></p>
        <input placeholder="  Username " type="text" name="username" class="index_forget_password_textfield_username"/>
        <br>
        <br>
        <input placeholder="  Email address " type="text" name="emailaddress" id="emailaddress"
               class="index_forget_password_textfield_emailaddress"/>
        <br>
        <br>
        <div class="site">
            <div class="errMsg" style="display: none">
            </div>
        </div>
        <button type="submit" id="btn_confirm" value="confirm">Send verification</button>
        <a href="index.jsp">
        <button class="btn_homepage" type="button" id="return" value="return">HomePage</button>
        </a>
    </div>
</form>
</div>
</body>
</html>