<%@ page import="DAO.ArticleDAO" %>
<%@ page import="POJO.ArticlePOJO" %>
<%@ page import="java.util.List" %>
<%@ page import="POJO.ImagePOJO" %>
<%@ page import="POJO.UserPOJO" %><%--
  Created by IntelliJ IDEA.
  User: kugn318
  Date: 18/10/2018
  Time: 4:08 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>All Media Gallery</title>
    <script type="text/javascript" src="../JQuery_lib/jquery-3.3.1.js"></script>
    <script type="text/javascript" src="../JS/myJS.js"></script>
    <script type="text/javascript" src="../JS/MediaGallery.js"></script>
    <link rel="stylesheet" href="../articleCSS/MediaGallery.css">

    <% UserPOJO userPOJO=(UserPOJO)request.getSession().getAttribute("userPOJO");%>
</head>
<body>

<div class="logo">
    <img id="index_logo" src="../images/icon_logo.png">
</div>

<div id="showModal" class="modal">
    <span class="close">×</span>
    <span class="previous"> < </span>
    <img class="imageModal" id="featuredImage">
    <span class="next"> > </span>
    <div id="caption">
        <h4 id="imageName"></h4>
        <p id="articleTitle"></p>
        <p id =alertBox>There are no more images to view!</p>
    </div>
</div>

<%
    try (ArticleDAO newArticleDAO = new ArticleDAO()) {
        List<ArticlePOJO> allArticles = newArticleDAO.loadAllArticles();

        out.println("<h3>All Images: </h3>");
        out.println("<div id=\"image_gallery\">");
        int count = 1;
        for (ArticlePOJO eachArticle: allArticles){
            List<ImagePOJO> images = newArticleDAO.loadImageFromArticle(eachArticle.getArticle_id());
            for (ImagePOJO singleImage: images){
                System.out.println(singleImage.getSource());
%>
<img class ="images" id="image_<%=count%>" alt="<%=eachArticle.getTitle()%>" src="../Uploaded_Images/<%=singleImage.getSource()%>" width="250" onclick="showImage('<%=singleImage.getSource()%>', '<%=count%>', '<%=eachArticle.getTitle()%>')">
<%
            count++;
            }
        }
    out.println("</div>");
    out.println("<br>");
    out.println("<hr>");

    out.println("<h3> All Audio: </h3>");
    out.println("<div id=\"audio_gallery\">");
    for (ArticlePOJO eachArticle: allArticles){
        if (eachArticle.getArticle_audio() != null){
            out.println("<audio controls>");
            out.println("<source class =\"audios\" src=\"../Uploaded_Audio/" + eachArticle.getArticle_audio() + "\" type=\"audio/mp3\">");
            out.println("</audio>");
%>
<br>
<b>File Name: <%=eachArticle.getArticle_audio()%></b>
<br>
<i>From Article: <%=eachArticle.getTitle()%></i>
<br>
<br>
<%
        }
    }
    out.println("</div>");
    out.println("<br>");
    out.println("<hr>");

    out.println("<h3>All Video: </h3>");
    out.println("<div id=\"video_gallery\">");
    for (ArticlePOJO eachArticle: allArticles) {
        if (eachArticle.getArticle_video() != null){
            out.println("<video width=\"280px\" height=\"200px\" controls>");
            out.println("<source class =\"videos\" src=\"../Uploaded_Video/" + eachArticle.getArticle_video() + "\" type=\"video/mp4\">");
            out.println("</video>");
%>
<br>
<i>From Article: <%=eachArticle.getTitle()%></i>
<br>
<br>
<%
            }
        }
        out.println("</div>");
        out.println("<br>");
        out.println("<hr>");
    }
    catch (Exception e){
    }

%>

<hr>


<%  if (userPOJO!=null) {
    out.println("<a href=\"../article/mediaGalleryUser.jsp\"><button class=\"btn_gallery_user_gallery\">Load User Media</button></a>");
}
%>

<a href="../index.jsp"><button class="btn_gallery_back">Go Back</button></a>

</body>
</html>
