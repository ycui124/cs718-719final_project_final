//Function that will show the thumbnail image that is clicked in a modal;
// also has inner functions that allow the user to scroll left and right to see the next image.

function showImage(source, id, title) {

    //if alert is showing, will hide it
    var alertBox = document.getElementById("alertBox");
    if (alertBox.style.display = "block"){
        alertBox.style.display = "none";
    }

    //gets the last and first ID of the images in the gallery
    var lastImage = document.getElementById("image_gallery").lastElementChild.id;
    var lastImageID = lastImage.replace(/image_/, "");
    var lastID = parseInt(lastImageID);
    var firstImage = document.getElementById("image_gallery").firstElementChild.id;
    var firstImageID = firstImage.replace(/image_/, "");
    var firstID = parseInt(firstImageID);

    //formats the name of the image and the article the image came from
    var imageName = source.replace(/.jpg/i, "").replace(/_/g, " ");
    var fromArticle = "From article: " + title;

    //displays the modal with the relevant image featured
    var modal = document.getElementById('showModal');
    var modalImg = document.getElementById("featuredImage");
    var captionText = document.getElementById("imageName");
    var articleTitle = document.getElementById("articleTitle");
    modal.style.display = "block";
    modalImg.src = "../Uploaded_Images/" + source;
    captionText.innerHTML = imageName;
    articleTitle.innerHTML = fromArticle;

    //if the next arrow is clicked, the next image will be displayed; if at end of gallery, will display alert message
    var next = document.getElementsByClassName("next")[0];
    next.onclick = function() {
        var imageID = parseInt(id);
        var nextImage = imageID + 1;

        if (nextImage <= lastID){
            var img = document.getElementById("image_" + nextImage + "");
            console.log("img: " + img);
            var title = img.getAttribute("alt");
            console.log("title: " + title);
            var s = img.getAttribute("src");
            var src = s.replace(/\..\/Uploaded_Images\//, "");
            alertBox.style.display = "none";
            showImage(src, nextImage, title);
        } else {
            alertBox.style.display = "block";
        }

    };

    //if the previous arrow is clicked, the previous image will be displayed; if at start of gallery, will display alert message
    var previous = document.getElementsByClassName("previous")[0];
    previous.onclick = function () {
        var imageID = parseInt(id);
        var previousImage = imageID - 1;
        console.log("previous: " + previousImage);

        var alertBox = document.getElementById("alertBox");

        if (previousImage >= firstID){
            var img = document.getElementById("image_" + previousImage + "");
            var title = img.getAttribute("alt");
            var s = img.getAttribute("src");
            var src = s.replace(/\..\/Uploaded_Images\//, "");
            alertBox.style.display = "none";
            showImage(src, previousImage, title);
        }  else {
            alertBox.style.display = "block";
        }
    };

    //if close button is clicked, the modal will close and back to main page
    var close = document.getElementsByClassName("close")[0];
    close.onclick = function () {
        modal.style.display = "none";
    };
}
